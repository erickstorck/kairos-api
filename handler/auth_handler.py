from config import settings
from functools import wraps
from aiohttp import web
import json
import jwt
import logging as log


def requires_permission(permission=""):
    def requires_permission_decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            try:
                token = get_token_auth_header(args[0])
                payload = verify_decode_jwt(token)

                if not check_permissions(permission, payload):
                    log.warn("Route not authorized {}.".format(permission))
                    return web.HTTPUnauthorized(text="Não autorizado")
                return f(*args, **kwargs)

            except jwt.ExpiredSignatureError:
                log.info("Token expired {}.".format(permission))
                raise web.HTTPForbidden(text="Token expirado")
            except json.decoder.JSONDecodeError:
                log.warn("Invalid Token {}.".format(permission))
                raise web.HTTPForbidden(text="Token inválido")
            except jwt.exceptions.InvalidSignatureError:
                log.warn("Invalid Signature Error.")
                raise web.HTTPForbidden(text="Assinatura inválida do Token.")
            except Exception as e:
                log.error(e)
                log.error("Token format invalid: {}.".format(permission))
                raise web.HTTPBadRequest(text="Formato token inválido!")

        return wrapper

    return requires_permission_decorator


def get_token_auth_header(request):
    if not request.headers.get("Authorization"):
        log.warn("Trying to access without token {}.".format(permission))
        return None
    else:
        auth = request.headers.get("Authorization")
        header_parts = auth.split(" ")
        if len(header_parts) != 2:
            return None
        elif header_parts[0].lower() != "bearer":
            return None
        return header_parts[1]


def verify_decode_jwt(token):
    try:
        secret = settings.jwt_secret
        decode = jwt.decode(
            token,
            secret,
            "HS256",
            options={"verify_signature": False},
        )
        return decode
    except jwt.ExpiredSignatureError as expired:
        log.error("expired")
        raise expired
    except json.decoder.JSONDecodeError as decode_error:
        log.error("decode error")
        raise decode_error
    except jwt.exceptions.InvalidSignatureError as invalid_signature:
        log.error("signature error")
        raise invalid_signature
    except Exception as e:
        log.error(e)
        log.error("another error")
        raise web.HTTPBadRequest()


def check_permissions(permition, payload) -> any:
    if "permission" in payload:
        if permition in payload["permission"]:
            return True
    return False
