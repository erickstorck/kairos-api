from models import Influx
import threading
import time
import os
from pathlib import Path
from datetime import datetime, timedelta

def threading_backup():
    session = Influx()
    client = session.client
    client.switch_database("collect_info")

    x = threading.Thread(target=verify_backup, args=())
    x.start()

    if x.is_alive:
        print('Backup thread running')

def verify_backup():
    Path(f"./backups").mkdir(exist_ok=True)
    influxObject = Influx()
    while True:
        try:
            backup_list = os.listdir('backups')
            if backup_list == []:
                influxObject.do_backup()
            else:
                last_backup = datetime.fromtimestamp(0)
                for backup in backup_list:
                    if last_backup < datetime.strptime(backup, '%y%m%d_%H%M%S'):
                        last_backup = datetime.strptime(backup, '%y%m%d_%H%M%S')
                if last_backup < (datetime.now() - timedelta(days=30)):
                    influxObject.do_backup()
            time.sleep(1800)                    
        except (Exception) as error:
            print('error verify_backup: ', error)
            thread_reconnect()

def thread_reconnect():
    time.sleep(30)
    threading_backup()