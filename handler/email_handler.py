from email.mime.text import MIMEText
import smtplib, ssl
from email.mime.multipart import MIMEMultipart
from config import settings
from models import Influx
import time
from datetime import datetime
import json

sender_email = settings.SMTP_EMAIL
password = settings.SMTP_PWD

def send_email_handler(text, email_list):
    if type(email_list) == str:
        email_list = email_list.split(',')
    
    if email_list == []:
        print('lista de email vazia')
        return
    html = ''
    alarmObject = { 
        'flag_falha_bateria': 'Bateria Anormal',
        'flag_alarme_temp': 'Alta Temperatura no Silo',
        'flag_falha_sensores': 'Falha Leitura Temperatura',
        'flag_falha_estacao_met': 'Falha Estação Meteorológica',
        'flag_falha_tx_rx': 'Falha de TX-RX',
        'flag_falha_comunicacao': 'Falha de Comunicação' 
    }

    warningObject = {
        'flag_hora_ponta': 'Horário de Ponta',
        'flag_umidade_alta': 'Umidade Ambiente Alta',
        'flag_chovendo': 'Chovendo',
        'flag_ambiente_quente': 'Ambiente quente',
        'flag_motor_ligado': 'Motor Aeração ligado',
        'flag_temperatura_ok': 'Temperatura Ok',
        'flag_relogio_reajustado': 'Relógio Reajustado',
        'flag_retorno_energia': 'Retorno da Energia'
    }
    flags = {'alarms': [], 'warning': []}

    if 'alarm_status' in text.keys() and 'silo' in text.keys():
        alarms = text['alarm_status'][text['silo']]
        for flag in alarms['activeFlags']:
            if flag in text['alarm_status']['enabledFlags']:
                if flag in warningObject.keys():
                    flags['warning'].append(warningObject[flag])
                elif flag in alarmObject.keys():
                    flags['alarms'].append(alarmObject[flag])
        
        html = f'''
            <p><h1><b>{text['silo']}</b> - {alarms['local']} - {alarms['horario']}</h1></p>
        '''
        if flags['warning'] != []:
            html += '<h3>Avisos:</h3>'
            for war in flags['warning']:
                html += f'<li>{war}</li>'
        
        if flags['alarms'] != []:
            html += '<h3>Alarmes:</h3>'
            for alarm in flags['alarms']:
                html += f'<li>{alarm}</li>'
        

    # for k,v in text.items():
    #     flags = ''
    #     for flag in v['activeFlags']:
    #         flags += flag + '<br>'
    #     if html == '':
    #         html = f'''
    #             <h1><b>{k}</b> - {v['horario']}</h1>
    #             <h2>Flags ativas: </h2>{flags}
    #         '''
    #     else:
    #         html += f'''
    #             <hr>
    #             <h1><b>{k}</b> - {v['horario']}</h1>
    #             <h2>Flags ativas: </h2>{flags}
    #         '''

    
    for receiver_email in email_list:
        message = MIMEMultipart("alternative")
        message["Subject"] = "Kairos Alert"
        message["From"] = sender_email
        message["To"] = receiver_email
        message.attach(MIMEText(html, "html"))

        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, password)
            server.sendmail(
                sender_email, receiver_email, message.as_string()
            )

def get_alarm_emails():
    try:
        with open('alarm_config.json', 'r') as f:
            alarm_config = json.load(f)
        return alarm_config['emails']
    except FileNotFoundError:
        with open('alarm_config.json', 'w') as f:
            json.dump({'check_interval': 60, 'emails': []}, f)
        return []

def add_alarm_email(email):
    try:
        with open('alarm_config.json', 'r') as f:
            alarm_config = json.load(f)

        if not email in alarm_config['emails']:
            alarm_config['emails'].append(email)

        with open('alarm_config.json', 'w') as f:
            json.dump(alarm_config, f)

    except FileNotFoundError:
        with open('alarm_config.json', 'w') as f:
            json.dump({'check_interval': 60, 'emails': [email]}, f)
    
    return True

def remove_alarm_email(email):
    try:
        with open('alarm_config.json', 'r') as f:
            alarm_config = json.load(f)

        alarm_config['emails'].remove(email)

        with open('alarm_config.json', 'w') as f:
            json.dump(alarm_config, f)

    except FileNotFoundError:
        with open('alarm_config.json', 'w') as f:
            json.dump({'check_interval': 60, 'emails': []}, f)
    
    return True

def set_new_enabled_alarms(enabled_alarms):
    try:
        with open('alarm_status.json', 'r') as f:
            alarm_status = json.load(f)
        alarm_status['enabledFlags'] = enabled_alarms
        print(alarm_status)

        with open('alarm_status.json', 'w') as f:
            json.dump(alarm_status, f)

    except FileNotFoundError:
        with open('alarm_status.json', 'w') as f:
            json.dump({"enabledFlags": []}, f)
    
    return True

def enabled_alarms():
    with open('alarm_status.json', 'r') as f:
        alarm_status = json.load(f)

    return alarm_status['enabledFlags']