from models import Influx, SiloConnection
from datetime import datetime, timedelta
from zoneinfo import ZoneInfo
import time
import threading
import json
from .email_handler import send_email_handler
from config import settings

silo_connection = SiloConnection()

def threading_collect():
    session = Influx()
    client = session.client
    client.switch_database("collect_info")

    x = threading.Thread(target=collect_data, args=())
    x.start()

    if x.is_alive:
        print('Collect thread running')


def get_silos():
    client = Influx().client
    client.switch_database("configs")
    silos = []
    # pendulos = {"silo_01": 6, "silo_02": 44, "silo_03": 32}
    # sensores = {"silo_01": 10, "silo_02": 13, "silo_03": 5}
    for config in client.query("select * from silos").get_points():
        silos.append(
            {
                "silo": config["silo"],
                "freq": config["freq"],
                "local": config['local']
                # "pendulos": pendulos[config["silo"]],
                # "sensores": sensores[config["silo"]],
            }
        )
    return silos

def collect_data():
    client = Influx().client
    json_payload = []
    client.switch_database("collect_info")
    thread = next(
                client.query("select * from thread order by time desc").get_points()
            )
    print(f'thread: {thread}')
    if not thread['autoStart']:
        data = {
        "measurement": "thread",
        "tags": {"value": 1},
        "time": 1632265794713453000,
        "fields": {"status": "running", "collect": "stop", "autoStart": False},
        }
        json_payload.append(data)
        client.write_points(json_payload)
    else:
        data = {
        "measurement": "thread",
        "tags": {"value": 1},
        "time": 1632265794713453000,
        "fields": {"status": "running", "collect": "start", "autoStart": True},
        }
        json_payload.append(data)
        client.write_points(json_payload)
    time.sleep(2)

    while True:
        try:
            silos = get_silos()
            client = Influx().client
            client.switch_database("collect_info")

            thread = next(
                client.query("select * from thread order by time desc").get_points()
            )

            if thread["collect"] == "start" and thread["status"] == "running":
                for silo in silos:
                    freq = silo['freq']
                    local = silo['local']
                    try:
                        print('coleta de dados iniciada on', silo)
                        data = silo_connection.coletar_data(freq)
                        if data == None or data == [] or data == '':
                            raise Exception('Falha de comunicação')
                        print('data: ', data)
                        json_data = parse_data(data, freq, local)
                        json_data[0]['tags']['flag_falha_comunicacao'] = False
                        print('json data: ', json_data)
                        json_data[0] = alarm_verify(silo['silo'], json_data[0])
                        client.switch_database(silo["silo"])
                        client.write_points(points=json_data, time_precision='m')
                        print("data inserted: ", json_data[0]['time'], silo)
                        time.sleep(1)
                    except (Exception) as error:
                        json_data = [{'measurement': 'general', 'tags': {'error': error.args[0], 'flag_falha_comunicacao': True, 'local': local}, 'time': datetime.now().astimezone(ZoneInfo('America/Sao_Paulo')), 'fields': {'temp_ambiente': 0.0, 'umidade': 0, 'max_temp': 0.0, 'med_umidade': 0, 'tensao_bat': 0.0}}]
                        print('json data: ', json_data)
                        alarm_verify(silo['silo'], json_data[0])
                        client.switch_database(silo["silo"])
                        client.write_points(points=json_data, time_precision='m')
                        print('error: ', error)
                        time.sleep(1)
                print(f'sleep time: {int(settings.SLEEP_TIME)}s')
                time.sleep(int(settings.SLEEP_TIME))
            else:
                print(f'sleep time: {int(settings.SLEEP_TIME)}s')
                time.sleep(int(settings.SLEEP_TIME))
        except Exception as error:
            print("Thread finalizada, error:", error)
            json_payload = []
            data = {
                "measurement": "thread",
                "tags": {"value": 1},
                "time": 1632265794713453000,
                "fields": {"status": "stopped", "collect": "stop"},
            }
            json_payload.append(data)
            client.write_points(json_payload)
            thread_reconnect()
            break

def alarm_verify(silo, json_data):
    alarm_status = {}
    activeFlags = []
    try:
        with open('alarm_config.json', 'r') as f:
            alarm_config = json.load(f)
    except FileNotFoundError:
        with open('alarm_config.json', 'w') as f:
            json.dump({'check_interval': 60, 'emails': []}, f)
    for k,v in json_data['tags'].items():
        try:
            with open('alarm_status.json', 'r') as f:
                alarm_status = json.load(f)
        except FileNotFoundError:
            with open('alarm_status.json', 'w') as f:
                json.dump({"enabledFlags": []}, f)
        # alarme ativado
        if v == True and isinstance(v, bool):
            if not k in activeFlags and k != 'flag_tipo_controle':
                activeFlags.append(k)
            # o alarme está habilitado?
            if k in alarm_status['enabledFlags']:
                # ja existe alarme nesse silo?
                if silo in alarm_status.keys():
                    # ja existe essa flag nesse silo?
                    if k in alarm_status[silo]['activeFlags']:
                        # o alarme já passou do tempo limite para repetir?
                        if datetime.strptime(alarm_status[silo]['horario'], '%d/%m/%y %H:%M:%S').replace(tzinfo=ZoneInfo('America/Sao_Paulo')) < (json_data['time'] - timedelta(minutes=alarm_config['check_interval'])):
                            alarm_status[silo]['horario'] = json_data['time'].strftime('%d/%m/%y %H:%M:%S')
                            send_email_handler({'alarm_status': alarm_status, 'silo': silo}, alarm_config['emails'])
                    # alarme novo (silo já existe)
                    else:
                        alarm_status[silo]['activeFlags'].append(k)
                        alarm_status[silo]['horario'] = json_data['time'].strftime('%d/%m/%y %H:%M:%S')
                        send_email_handler({'alarm_status': alarm_status, 'silo': silo}, alarm_config['emails'])
                # alarme novo (silo não existe)
                else:
                    alarm_status[silo] = {'activeFlags' : [k]}
                    alarm_status[silo]['horario'] = json_data['time'].strftime('%d/%m/%y %H:%M:%S')
                    alarm_status[silo]['local'] = json_data['tags']['local']
                    send_email_handler({'alarm_status': alarm_status, 'silo': silo}, alarm_config['emails'])

                # salva arquivo de alarme
                with open('alarm_status.json', 'w') as f:
                        json.dump(alarm_status, f)
        # silo não está alarmando mas tem alarmes
        elif silo in alarm_status.keys():
            if k in activeFlags:
                activeFlags.remove(k)
            # flag existe nos alarmes ativos
            if k in alarm_status[silo]['activeFlags']:
                # remove flag
                alarm_status[silo]['activeFlags'].remove(k)
                # salva arquivo de alarme
                with open('alarm_status.json', 'w') as f:
                    json.dump(alarm_status, f)
    
    json_data['tags']['activeFlags'] = activeFlags
    return json_data

def parse_data(data, freq, local):
    qtd_silo = {
        1: {"tamanho": "9m", "modelo": 1},
        3: {"tamanho": "11m", "modelo": 2},
        4: {"tamanho": "13m", ",modelo": 3},
        5: {"tamanho": "15m", "modelo": 4},
        6: {"tamanho": "16-18m", "modelo": 5},
        7: {"tamanho": "20m", "modelo": 6},
        9: {"tamanho": "22m", "modelo": 7},
        12: {"tamanho": "24,7m", "modelo": 8},
        14: {"tamanho": "27,2m", "modelo": 9},
        15: {"tamanho": "28m", "modelo": 10},
        21: {"tamanho": "31m", "modelo": 11},
        32: {"tamanho": "33m", "modelo": 12},
        44: {"tamanho": "40m", "modelo": 13},
    }
    if f'{data[-3]}/{data[-4]}/{data[-5]} {data[-2]}:{data[-1]}' == '0/0/0 0:0':
        time = datetime.fromtimestamp(10800)
    else:
        time = datetime.strptime(f'{data[-3]}/{data[-4]}/{data[-5]} {data[-2]}:{data[-1]}', '%y/%m/%d %H:%M').replace(tzinfo=ZoneInfo('America/Sao_Paulo'))
    json_payload = []
    silosGeneral = {
        "measurement": "general",
        "tags": {
            "max_temp_motor": int(data[5]),
            "max_umi": int(data[6]),
            "alarm_max_temp": int(data[7]),
            "produto": int(data[8]),
            "mode": int(data[9]),
            "potencia_motor": int(data[10]),
            "flag_falha_tx_rx": data[11],
            "flag_ambiente_quente": data[12],
            "flag_falha_estacao_met": data[13],
            "flag_umidade_alta": data[14],
            "flag_chovendo": data[15],
            "flag_temperatura_ok": data[16],
            "flag_hora_ponta": data[17],
            "flag_falha_bateria": data[18],
            "flag_motor_ligado": data[19],
            "flag_tipo_controle": data[20],
            "flag_sensor_chuva": data[21],
            "flag_alarme_temp": data[22],
            "flag_falha_sensores": data[23],
            "flag_relogio_reajustado": data[24],
            "flag_retorno_energia": data[25],
            "modelo": qtd_silo[int(data[26])]['modelo'],
            "qtd_pen": int(data[26]),
            "tamanho": qtd_silo[int(data[26])]["tamanho"],
            "freq": freq,
            "local": local
        },
        "time": time,
        "fields": {
            "temp_ambiente": float(data[0]),
            "umidade": int(data[1]),
            "max_temp": float(data[2]),
            "med_umidade": int(data[3]),
            "tensao_bat": float(data[4]),
        },
    }
    json_payload.append(silosGeneral)

    for i in range(1, int(data[26])+1):
        measure = f"pendulo_{i}" if i > 9 else f"pendulo_0{i}"
        silosData = {
            "measurement": measure,
            "tags": {
                "sensor_01": str(data[28 + (32 * (i-1))]),
                "sensor_02": str(data[30+ (32 * (i-1))]),
                "sensor_03": str(data[32+ (32 * (i-1))]),
                "sensor_04": str(data[34+ (32 * (i-1))]),
                "sensor_05": str(data[36+ (32 * (i-1))]),
                "sensor_06": str(data[38+ (32 * (i-1))]),
                "sensor_07": str(data[40+ (32 * (i-1))]),
                "sensor_08": str(data[42+ (32 * (i-1))]),
                "sensor_09": str(data[44+ (32 * (i-1))]),
                "sensor_10": str(data[46+ (32 * (i-1))]),
                "sensor_11": str(data[48+ (32 * (i-1))]),
                "sensor_12": str(data[50+ (32 * (i-1))]),
                "sensor_13": str(data[52+ (32 * (i-1))]),
                "sensor_14": str(data[54+ (32 * (i-1))]),
                "sensor_15": str(data[56+ (32 * (i-1))]),
                "sensor_16": str(data[58+ (32 * (i-1))])
            },
            "time": time,
            "fields": {
                "temp_01": float(data[27+ (32 * (i-1))]),
                "temp_02": float(data[29+ (32 * (i-1))]),
                "temp_03": float(data[31+ (32 * (i-1))]),
                "temp_04": float(data[33+ (32 * (i-1))]),
                "temp_05": float(data[35+ (32 * (i-1))]),
                "temp_06": float(data[37+ (32 * (i-1))]),
                "temp_07": float(data[39+ (32 * (i-1))]),
                "temp_08": float(data[41+ (32 * (i-1))]),
                "temp_09": float(data[43+ (32 * (i-1))]),
                "temp_10": float(data[45+ (32 * (i-1))]),
                "temp_11": float(data[47+ (32 * (i-1))]),
                "temp_12": float(data[49+ (32 * (i-1))]),
                "temp_13": float(data[51+ (32 * (i-1))]),
                "temp_14": float(data[53+ (32 * (i-1))]),
                "temp_15": float(data[55+ (32 * (i-1))]),
                "temp_16": float(data[57+ (32 * (i-1))])
            },
        }
        json_payload.append(silosData)
    return json_payload

def change_check_interval(value):
    try:
        with open('alarm_config.json', 'r') as f:
            alarm_config = json.load(f)
        
        alarm_config['check_interval'] = value
        with open('alarm_config.json', 'w') as f:
            json.dump(alarm_config, f)

    except FileNotFoundError:
        with open('alarm_config.json', 'w') as f:
            json.dump({'check_interval': value}, f)
    
    return True

def get_check_interval():
    try:
        with open('alarm_config.json', 'r') as f:
            alarm_status = json.load(f)
        
        return alarm_status['check_interval']
    except FileNotFoundError:
        return 0

def thread_reconnect():
    time.sleep(30)
    threading_collect()


# def generator(sensores, pendulos, freq):
#     modelo_silo = {
#         1: {"tamanho": "9m", "modelo": 1},
#         3: {"tamanho": "11m", "modelo": 2},
#         4: {"tamanho": "13m", ",modelo": 3},
#         5: {"tamanho": "15m", "modelo": 4},
#         6: {"tamanho": "16-18m", "modelo": 5},
#         7: {"tamanho": "20m", "modelo": 6},
#         9: {"tamanho": "22m", "modelo": 7},
#         12: {"tamanho": "24,7m", "modelo": 8},
#         14: {"tamanho": "27,2m", "modelo": 9},
#         15: {"tamanho": "28m", "modelo": 10},
#         21: {"tamanho": "31m", "modelo": 11},
#         32: {"tamanho": "33m", "modelo": 12},
#         44: {"tamanho": "40m", "modelo": 13},
#     }
#     json_payload = []
#     silosGeneral = {
#         "measurement": "general",
#         "tags": {
#             "max_temp_motor": 50,
#             "max_umi": 65,
#             "alarm_max_temp": 30,
#             "produto": 1,
#             "mode": 1,
#             "potencia_motor": 100,
#             "flag_falha_tx_rx": False,
#             "flag_ambiente_quente": False,
#             "flag_falha_estacao_met": False,
#             "flag_umidade_alta": False,
#             "flag_chovendo": False,
#             "flag_temperatura_ok": True,
#             "flag_hora_ponta": False,
#             "flag_falha_bateria": False,
#             "flag_motor_ligado": False,
#             "flag_tipo_controle": False,
#             "flag_sensor_chuva": False,
#             "flag_alarme_temp": False,
#             "flag_falha_sensores": False,
#             "modelo": modelo_silo[pendulos]["modelo"],
#             "tamanho": modelo_silo[pendulos]["tamanho"],
#             "qtd_pen": pendulos,
#             "freq": freq,
#         },
#         # "time": datetime.now(),
#         "fields": {
#             "temp_ambiente": random.randint(180, 300) / 10,
#             "umidade": random.randint(350, 650) / 10,
#             "med_umidade": random.randint(350, 650) / 10,
#             "max_temp": random.randint(180, 300) / 10,
#             "tensao_bat": random.randint(110, 120) / 10,
#         },
#     }
#     arr = []
#     for i in range(1, 17):
#         if i > sensores:
#             arr.append(False)
#         else:
#             arr.append(True)
#     json_payload.append(silosGeneral)
#     for i in range(1, pendulos + 1):
#         silosData = {
#             "measurement": f"pendulo_{i}",
#             "tags": {
#                 "sensor_01": arr[0],
#                 "sensor_02": arr[1],
#                 "sensor_03": arr[2],
#                 "sensor_04": arr[3],
#                 "sensor_05": arr[4],
#                 "sensor_06": arr[5],
#                 "sensor_07": arr[6],
#                 "sensor_08": arr[7],
#                 "sensor_09": arr[8],
#                 "sensor_10": arr[9],
#                 "sensor_11": arr[10],
#                 "sensor_12": arr[11],
#                 "sensor_13": arr[12],
#                 "sensor_14": arr[13],
#                 "sensor_15": arr[14],
#                 "sensor_16": arr[15],
#             },
#             # "time": datetime.now(),
#             "fields": {
#                 "temp_01": random.randint(180, 300) / 10,
#                 "temp_02": random.randint(180, 300) / 10,
#                 "temp_03": random.randint(180, 300) / 10,
#                 "temp_04": random.randint(180, 300) / 10,
#                 "temp_05": random.randint(180, 300) / 10,
#                 "temp_06": random.randint(180, 300) / 10,
#                 "temp_07": random.randint(180, 300) / 10,
#                 "temp_08": random.randint(180, 300) / 10,
#                 "temp_09": random.randint(180, 300) / 10,
#                 "temp_10": random.randint(180, 300) / 10,
#                 "temp_11": random.randint(180, 300) / 10,
#                 "temp_12": random.randint(180, 300) / 10,
#                 "temp_13": random.randint(180, 300) / 10,
#                 "temp_14": random.randint(180, 300) / 10,
#                 "temp_15": random.randint(180, 300) / 10,
#                 "temp_16": random.randint(180, 300) / 10,
#             },
#         }
#         json_payload.append(silosData)
#     return json_payload
