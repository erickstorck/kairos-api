from handler import threading_collect, requires_permission
from aiohttp import web
from models import Influx
import json


class CollectController:

    async def clear_alarms(request):
        try:
            with open('alarm_status.json', 'w') as f:
                json.dump({"enabledFlags": []}, f)
            return web.HTTPOk()
        except Exception as error:
            print("error do_collect: ", error)
            return web.HTTPBadRequest(text="payload problem")


    async def status(request):
        try:
            influxObject = Influx()
            client = influxObject.client
            client.switch_database("collect_info")
            if thread := next(client.query("select * from thread").get_points()):
                del thread["value"]
                del thread["time"]
                return web.json_response(thread)
            return web.json_response({})
        except (Exception) as error:
            print("error status: ", error)
            return web.HTTPBadRequest(text="payload problem")
    
    @requires_permission("admin")
    async def toggle_status(request):
        try:
            influxObject = Influx()
            client = influxObject.client
            client.switch_database("collect_info")
            thread = next(client.query("select * from thread").get_points())
            if thread["status"] == "stopped":
                threading_collect()

            payload = await request.json()
            collect = payload.get("collect")
            autoStart = payload.get("autoStart")
            json_payload = []
            data = {
                "measurement": "thread",
                "tags": {"value": 1},
                "time": 1632265794713453000,
                "fields": {"collect": collect, 'autoStart': autoStart},
            }
            json_payload.append(data)
            client.write_points(json_payload)
            return web.HTTPOk()
        except Exception as error:
            print("error toggle_status: ", error)
            return web.HTTPBadRequest(text="payload problem")
