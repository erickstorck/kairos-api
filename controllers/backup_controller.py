from handler import requires_permission
from models import Influx
from aiohttp import web
import os

class BackupController:
    @requires_permission("admin")
    async def backup(request):
        try:
            influxObject = Influx()
            influxObject.do_backup()
            return web.HTTPOk()
        except Exception as error:
            print("error backup: ", error)
            return web.HTTPBadRequest(text="payload problem")
    
    @requires_permission("admin")
    async def restore(request):
        try:
            payload = await request.json()
            if backup := payload['backup']:
                influxObject = Influx()
                influxObject.do_restore(backup)
            return web.HTTPOk()
        except Exception as error:
            print("error backup: ", error)
            return web.HTTPBadRequest(text="payload problem")
    
    async def list(request):
        try:
            return web.json_response(os.listdir('./backups'))
        except Exception as error:
            print("error list: ", error)
            return web.HTTPBadRequest(text="payload problem")

    async def flash_restore(request):
        try:
            influxObject = Influx()
            influxObject.flash_drive_restore()
            return web.HTTPOk()
        except Exception as error:
            print("error list: ", error)
            return web.HTTPBadRequest(text="payload problem")
    
    @requires_permission("admin")
    async def delete(request):
        try:
            if backup := request.match_info.get("backup"):
                influxObject = Influx()
                influxObject.delete(backup)
                return web.HTTPOk()
            else:
                return web.json_response(data=[], status=404)
        except Exception as error:
            print("error delete_config: ", error)
            return web.HTTPBadRequest(text="payload problem")
