from aiohttp import web
from models import User
import bcrypt
from uuid import uuid4
from config import settings
from handler import requires_permission


class UserController:
    async def login(request):
        try:
            payload = await request.json()
            # passwd = payload.get("password").encode("utf-8")
            # hashed_password = bcrypt.hashpw(passwd, bcrypt.gensalt())
            userObject = User()
            if token := userObject.login(payload.get("email"), payload.get("password")):
                return web.json_response({"token": token}, status=200)
            else:
                return web.HTTPNotAcceptable(text="Wrong email/password")
        except (Exception) as error:
            print('error login: ', error)
            return web.HTTPBadRequest(text='error')

    async def logout(request):
        try:
            user = None
            userObject = User()
            payload = await request.json()
            if user_email := payload.get("email"):
                if user := userObject.user_check(user_email):
                    user["token"] = ""
                else:
                    return web.HTTPBadRequest(text="Email não encontrado")
            else:
                return web.HTTPBadRequest(text="Email deve ser informado")

            if userObject.update(user):
                return web.HTTPOk()
            else:
                return web.HTTPBadRequest(text="User not updated")
        except (Exception) as error:
            print('error logout: ', error)
            return web.HTTPBadRequest(text="payload problem")
    
    @requires_permission("admin")
    async def create(request):
        try:
            user = None
            userObject = User()
            payload = await request.json()
            if user_email := payload.get("email"):
                if user := userObject.user_check(user_email):
                    return web.HTTPNotAcceptable(text="Email já cadastrado")
            else:
                return web.HTTPBadRequest(text="Email deve ser informado")

            name = "User"
            if user_name := payload.get("name"):
                name = user_name
            else:
                return web.HTTPBadRequest(text="Nome deve ser informado")

            role = "default"
            if user_role := payload.get("role"):
                role = user_role

            user_id = str(uuid4())
            password = settings.DFLT_PWD.encode("utf-8")
            hashed_password = bcrypt.hashpw(password, bcrypt.gensalt())
            created = userObject.create(
                id=user_id,
                name=name,
                email=user_email,
                password=hashed_password.decode("utf-8"),
                role=role,
                token=userObject.generate_token(role, user_email, name, True),
                dflt_pwd=True,
            )
            if created:
                resp = {
                    "email": user_email,
                    "temp_password": password.decode("utf-8"),
                }
                return web.json_response(resp, status=201)
            else:
                return web.HTTPError(text="user creation problem")
        except (Exception) as error:
            print('error create: ', error)
            return web.HTTPBadRequest(text="payload problem")
    
    async def update(request):
        try:
            user = None
            userObject = User()
            payload = await request.json()
            if user_email := payload.get("email"):
                if user := userObject.user_check(user_email):
                    if user_passwd := payload.get("password"):
                        hashed_password = bcrypt.hashpw(
                            user_passwd.encode("utf-8"), bcrypt.gensalt()
                        )
                        user["password"] = hashed_password.decode("utf-8")
                        user["dflt_pwd"] = False
                    if user_name := payload.get("name"):
                        user["name"] = user_name

                    if role := payload.get("role"):
                        user["token"] = userObject.generate_token(
                            role, user_email, user_name, user["dflt_pwd"]
                        )
                        user["role"] = role
                else:
                    return web.HTTPBadRequest(text="Email não encontrado")
            else:
                return web.HTTPBadRequest(text="Email deve ser informado")

            if userObject.update(user):
                return web.HTTPOk()
            else:
                return web.HTTPBadRequest(text="User not updated")
        except (Exception) as error:
            print('error update: ', error)
            return web.HTTPBadRequest(text="error")
    
    @requires_permission("admin")
    async def reset(request):
        try:
            user = None
            userObject = User()
            payload = await request.json()
            if user_email := payload.get("email"):
                if user := userObject.user_check(user_email):
                    hashed_password = bcrypt.hashpw(
                        settings.DFLT_PWD.encode("utf-8"), bcrypt.gensalt()
                    )
                    user["password"] = hashed_password.decode("utf-8")
                    user["dflt_pwd"] = True
                else:
                    return web.HTTPBadRequest(text="Email não encontrado")
            else:
                return web.HTTPBadRequest(text="Email deve ser informado")

            if userObject.update(user):
                return web.HTTPOk()
            else:
                return web.HTTPBadRequest(text="User not updated")
        except (Exception) as error:
            print('error reset: ', error)
            return web.HTTPBadRequest(text="payload problem")

    @requires_permission("admin")
    async def delete(request):
        try:
            userObject = User()
            if user_id := request.match_info.get("id"):
                userObject.delete(user_id)
                return web.HTTPOk()
            else:
                return web.json_response(data=[], status=404)
        except (Exception) as error:
            print('error delete: ', error)
            return web.HTTPBadRequest(text="payload problem")

    @requires_permission("admin")
    async def by_email(request):
        try:
            user = None
            userObject = User()
            payload = await request.json()
            if user_email := payload.get("email"):
                return web.json_response(userObject.user_check(user_email), status=200)
            else:
                return web.HTTPBadRequest(text="Email deve ser informado")
        except (Exception) as error:
            print('error by_email: ', error)
            return web.HTTPBadRequest(text="payload problem")

    @requires_permission("admin")
    async def all(request):
        try:
            userObject = User()
            return web.json_response(userObject.get_all(), status=200)
        except (Exception) as error:
            print('error all: ', error)
            return web.HTTPBadRequest(text="payload problem")
