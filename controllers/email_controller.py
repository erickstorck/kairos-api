from aiohttp import web
from handler import send_email_handler, change_check_interval, get_check_interval, add_alarm_email, remove_alarm_email, get_alarm_emails, set_new_enabled_alarms, enabled_alarms, requires_permission

class EmailController:
    async def send_email(request):
        try:
            payload = await request.json()
            if message := payload['message']:
                if target := payload['target']:
                    send_email_handler(message, target)
            return web.HTTPOk()
        except Exception as error:
            print("error send_email: ", error)
            return web.HTTPBadRequest(text="payload problem")
    
    @requires_permission("admin")
    async def change_check_interval(request):
        try:
            payload = await request.json()
            if check_interval := payload['check_interval']:
                if change_check_interval(check_interval):
                    return web.HTTPOk()
        except Exception as error:
            print("error change_check_interval: ", error)
            return web.HTTPBadRequest(text="payload problem")
    
    async def get_check_interval(request):
        try:
            if check_interval := get_check_interval():
                return web.json_response({'check_interval':check_interval})
            return web.HTTPBadRequest(text="get_check_interval problem")
        except Exception as error:
            print("error get_check_interval: ", error)
            return web.HTTPBadRequest(text="payload problem")

    async def get_alarm_emails(request):
        try:
            if emails := get_alarm_emails():
                return web.json_response(emails)
            if emails == []:
                return web.json_response([])
            return web.HTTPBadRequest(text="get_alarm_emails problem")
        except Exception as error:
            print("error get_alarm_emails: ", error)
            return web.HTTPBadRequest(text="payload problem")

    @requires_permission("admin")
    async def add_alarm_email(request):
        try:
            payload = await request.json()
            if email := payload['email']:
                if add_alarm_email(email):
                    return web.HTTPOk()
        except Exception as error:
            print("error add_alarm_email: ", error)
            return web.HTTPBadRequest(text="payload problem")
    
    @requires_permission("admin")
    async def remove_alarm_email(request):
        try:
            payload = await request.json()
            if email := payload['email']:
                if remove_alarm_email(email):
                    return web.HTTPOk()
        except Exception as error:
            print("error remove_alarm_email: ", error)
            return web.HTTPBadRequest(text="payload problem")

    @requires_permission("admin")
    async def set_enabled_alarms(request):
        try:
            payload = await request.json()
            enabled_alarms = payload['enabled_alarms']
            if set_new_enabled_alarms(enabled_alarms):
                return web.HTTPOk()
        except Exception as error:
            print("error set_enabled_alarms: ", error)
            return web.HTTPBadRequest(text="payload problem")
    
    async def get_enabled_alarms(request):
        try:
            return web.json_response(enabled_alarms())
        except Exception as error:
            print("error get_enabled_alarms: ", error)
            return web.HTTPBadRequest(text="payload problem")
        
    