from aiohttp import web
from models import Silo
from handler import requires_permission


class SiloController:
    async def silo(request):
        try:
            payload = await request.json()
            if silo := payload.get("silo"):
                return web.json_response(Silo.get_silo(silo), status=200)
        except Exception as error:
            print("error silo: ", error)
            return web.HTTPBadRequest(text="payload problem")

    async def all_configs(request):
        try:
            return web.json_response(Silo.get_all_configs(), status=200)
        except Exception as error:
            print("error all_configs: ", error)
            return web.HTTPBadRequest(text="payload problem")
    
    async def get_localization(request):
        try:
            return web.json_response(Silo.get_localization(), status=200)
        except Exception as error:
            print("error: ", error)
            return web.HTTPBadRequest(text="payload problem")

    @requires_permission("admin")    
    async def post_localization(request):
        try:
            payload = await request.json()
            if local := payload.get("local"):
                Silo.post_localization(local)
                return web.HTTPOk()
            else:
                return web.HTTPError()
        except Exception as error:
            print("error post_localization: ", error)
            return web.HTTPBadRequest(text="payload problem")

    @requires_permission("admin")
    async def new_config(request):
        try:
            payload = await request.json()
            silo = payload.get("silo")
            freq = payload.get("freq")
            local = payload.get("local")
            if Silo.set_new_config(silo, freq, local):
                return web.HTTPOk()
            else:
                return web.HTTPError()
        except Exception as error:
            print("error new_config: ", error)
            return web.HTTPBadRequest(text="payload problem")

    @requires_permission("admin")
    async def delete_config(request):
        try:
            if silo := request.match_info.get("silo"):
                Silo.delete(silo)
                return web.HTTPOk()
            else:
                return web.json_response(data=[], status=404)
        except Exception as error:
            print("error delete_config: ", error)
            return web.HTTPBadRequest(text="payload problem")

    async def get_sensor_data(request):
        try:
            payload = await request.json()
            silo = payload.get("silo")
            gte = payload.get("gte")
            lte = payload.get("lte")
            response = Silo.get_sensor_data(silo, gte, lte)
            return web.json_response(response)
        except Exception as error:
            print("error get_sensor_data: ", error)
            return web.HTTPBadRequest(text="payload problem")

    async def get_general_data(request):
        try:
            payload = await request.json()
            silo = payload.get("silo")
            gte = payload.get("gte")
            lte = payload.get("lte")
            response = Silo.get_general_data(silo, gte, lte)
            return web.json_response(response)
        except Exception as error:
            print("error get_general_data: ", error)
            return web.HTTPBadRequest(text="payload problem")
