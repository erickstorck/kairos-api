from aiohttp import web
from dynaconf.loaders import settings_loader
from models import User
import jwt
import json
from uuid import uuid4
from datetime import datetime
from config import settings


class TokenController:
    async def check_token(request):
        try:
            token = None
            userObject = User()
            payload = await request.json()
            token = payload["token"]
            if userObject.check_token(token):
                return web.HTTPOk()
            return web.HTTPBadRequest(text="Token inválido!")
        except jwt.ExpiredSignatureError:
            return web.HTTPUnauthorized(text="Token Expirado!")
        except json.decoder.JSONDecodeError:
            return web.HTTPBadRequest(text="Formato token inválido!")
        except Exception as error:
            print("error check_token: ", error)
            return web.HTTPBadRequest()
