from aiohttp import web
from aiohttp.web import json_response
import aiohttp_cors
from controllers import (
    UserController,
    TokenController,
    SiloController,
    CollectController,
    EmailController,
    BackupController
)


def setup_routes(app):
    cors = aiohttp_cors.setup(
        app,
        defaults={
            "*": aiohttp_cors.ResourceOptions(
                allow_credentials=True,
                expose_headers="*",
                allow_headers="*",
                allow_methods=["GET", "POST", "PUT", "DELETE", "OPTIONS"],
            )
        },
    )

    async def status(request):
        return json_response({"status": "healthy"})

    app.add_routes(
        [
            # auth / users
            web.get("/status", status),
            web.post("/user/create", UserController.create),
            web.delete("/user/{id}", UserController.delete),
            web.post("/user/update", UserController.update),
            web.post("/user/reset_pwd", UserController.reset),
            web.post("/user", UserController.by_email),
            web.post("/users", UserController.all),
            web.post("/login", UserController.login),
            web.post("/logout", UserController.logout),
            web.post("/token/check", TokenController.check_token),
            # silo config
            web.post("/silo", SiloController.silo),
            web.get("/silos/config", SiloController.all_configs),
            web.post("/silo/config", SiloController.new_config),
            web.get("/silos/localization", SiloController.get_localization),
            web.post("/silos/localization", SiloController.post_localization),
            web.delete("/silo/{silo}", SiloController.delete_config),
            # collect
            web.post("/collect", CollectController.toggle_status),
            web.get("/collect", CollectController.status),
            web.get("/clear_alarms", CollectController.clear_alarms),
            # data
            web.post("/sensor/data", SiloController.get_sensor_data),
            web.post("/general/data", SiloController.get_general_data),
            # email
            web.post("/email", EmailController.send_email),
            web.post("/check_interval", EmailController.change_check_interval),
            web.get("/check_interval", EmailController.get_check_interval),
            web.get("/alarm_emails", EmailController.get_alarm_emails),
            web.post("/add_alarm_email", EmailController.add_alarm_email),
            web.post("/remove_alarm_email", EmailController.remove_alarm_email),
            # alerts
            web.get("/enabled_alarms", EmailController.get_enabled_alarms),
            web.post("/enabled_alarms", EmailController.set_enabled_alarms),
            # backup
            web.get("/backup", BackupController.backup),
            web.post("/restore", BackupController.restore),
            web.get("/backup/list", BackupController.list),
            web.get("/restore/flash", BackupController.flash_restore),
            web.delete("/backup/delete/{backup}", BackupController.delete)
        ]
    )

    for route in list(app.router.routes()):
        cors.add(route)

    return app
