## Running

### To configure development environment, on terminal, run:

* create python virtual environment
    ```terminal
    python -m venv .env
    ```

* activate it
    ```terminal
    . .env/bin/activate
    ```

* install dependencies
    ```terminal
    pip install -r requirements.txt
    ```
---
### To build docker environment:

* build the image
    ``` docker
    docker build --rm -f dockerfile -t registry.gitlab.com/erickstorck/kairos-api:rasp .
    ```

* run it
    ``` docker
    docker run --rm -it -p 9000:9000 --name kairos-api registry.gitlab.com/erickstorck/kairos-api:rasp
    ```
* run it - permanently
    ``` docker
    docker run -d --restart unless-stopped -p 192.168.0.100:9000:9000 --name kairos-api registry.gitlab.com/erickstorck/kairos-api:latest
    ```

* push it
    ``` docker
    docker push registry.gitlab.com/erickstorck/kairos-api:latest
    ```

docker run -d --restart=always -v /home/pi/kairos-api/backups:/home/backup -p 192.168.0.100:8086:8086 --name influx influxdb:1.8
influxd restore -portable /home/backup/
influxd backup -portable /home/backup/