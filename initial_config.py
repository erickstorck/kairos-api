from datetime import datetime
from uuid import uuid4
import bcrypt
from models import User, Influx
from config import settings

def admin_setup():
    influxObj = Influx()
    influxObj.create_databases()
