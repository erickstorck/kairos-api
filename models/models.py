from logging import critical
from influxdb import InfluxDBClient
from config import settings
from datetime import datetime, timedelta
from zoneinfo import ZoneInfo
import bcrypt
from uuid import uuid4
import jwt
import json
import time
import math
import serial
import RPi.GPIO as GPIO
import subprocess
from pathlib import Path

class User:
    session = None
    
    def __init__(self, session=None):
        if session == None:
            session = Influx()
        self.session = session
        user = self.user_check("admin@kairos.com.br")
        if user == None:
            user_id = str(uuid4())
            password = settings.DFLT_PWD.encode("utf-8")
            hashed_password = bcrypt.hashpw(password, bcrypt.gensalt())
            created = self.create(
                id=user_id,
                name="Admin",
                email="admin@kairos.com.br",
                password=hashed_password.decode("utf-8"),
                role="admin",
                token=self.generate_token("admin", "admin@kairos.com.br", "Admin", True),
                dflt_pwd=True,
            )

    def user_check(self, email):
        return self.session.get_user(email)

    def login(self, email, passwd):
        return self.session.login(email, passwd)

    def update(self, user):
        return self.session.update(user)

    def get_all(self):
        if users := self.session.get_all_users():
            return users
        else:
            return []

    def delete(self, id):
        try:
            self.session.remove_user(id)
            return True
        except Exception as error:
            print(error)
            return False

    def create(self, id, name, email, password, role, token, dflt_pwd):
        try:
            self.session.new_user(id, name, email, password, role, token, dflt_pwd)
            return True
        except Exception as error:
            print(error)
            return False

    def generate_token(self, permission, email, name, dflt_pwd):
        try:
            payload = {}
            payload["permission"] = permission
            payload["email"] = email
            payload["name"] = name
            payload["dflt_pwd"] = dflt_pwd

            payload["exp"] = datetime.now() + timedelta(
                seconds=settings.EXP_TOKEN_SECONDS
            )
            token_jwt = jwt.encode(
                payload,
                settings.jwt_secret,
                algorithm="HS256",
            )
            return token_jwt
        except Exception as error:
            print(error)
            return ""

    def check_token(self, token):
        try:
            secret = settings.jwt_secret
            decode = jwt.decode(token, secret, "HS256")
            # print(decode) # uncomment for debug-only
            if self.session.check_token(token):
                return True
            else:
                return False
        except jwt.ExpiredSignatureError as expired:
            raise expired
        except json.decoder.JSONDecodeError as decode_error:
            raise decode_error
        except Exception as error:
            print(error)
            raise error


class Influx:
    client = InfluxDBClient(
        host=settings.DB_HOST,
        port=settings.DB_PORT,
        username=settings.DB_USER,
        password=settings.DB_PWD,
    )

    user = None

    def __init__(self, user=None):
        if user == None:
            self.user = User(self)
        
    def delete(self, backup):
        p = subprocess.Popen(
            f'sudo rm -rf backups/{backup}'.split(" "),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            stdin=subprocess.PIPE,
        )
        for comm in p.communicate():
            print(comm.decode())
    
    def create_databases(self):
        self.client.create_database("auth")
        self.client.create_database("collect_info")
        self.client.create_database("alarm_info")
        self.client.create_database("configs")
        self.client.switch_database("configs")
        for config in self.client.query("select * from silos").get_points():
            self.client.create_database(config["silo"])

    def remove_databases(self):
        print('removing all databases')        
        self.client.drop_database("configs")
        self.client.drop_database("alarm_info")
        self.client.drop_database("collect_info")
        self.client.drop_database("auth")
        for db in self.client.query("show databases").get_points():
            if db['name'] != 'internal':
                self.client.drop_database(db['name'])
        return True
        
    def do_restore(self, backup):
        if self.remove_databases():
            p = subprocess.Popen(
            f'docker exec influx influxd restore -portable /home/backup/{backup}'.split(" "),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            stdin=subprocess.PIPE,
            )
            for comm in p.communicate():
                print(comm.decode())
    
    def do_backup(self):
        file_name = datetime.now().astimezone(ZoneInfo('America/Sao_Paulo')).strftime('%y%m%d_%H%M%S')
        Path(f"./backups/{file_name}").mkdir(exist_ok=True)
        print(f'backing up db {file_name}')
        time.sleep(2)
        p = subprocess.Popen(
        f'docker exec influx influxd backup -portable /home/backup/{file_name}'.split(" "),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        stdin=subprocess.PIPE,
        )
        for comm in p.communicate():
            print(comm.decode())
        
        time.sleep(2)
        self.flash_drive_backup()
    
    def flash_drive_restore(self):
        for device in ['sda1', 'sdb1', 'sdc1', 'sdd1']:
            p = subprocess.Popen(
                f'sudo mount /dev/{device} /mnt/usb1 -o noauto,users,rw,umask=0'.split(" "),
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                stdin=subprocess.PIPE,
            )
            if not 'does not exist' in p.communicate()[1].decode():
                print(device, 'passed')
                break

        subprocess.Popen(
            f'sudo cp -R /mnt/usb1/backups /home/pi/kairos-api'.split(" "),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            stdin=subprocess.PIPE,
        )
        subprocess.Popen(
            f'sudo umount /mnt/usb1'.split(" "),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            stdin=subprocess.PIPE,
        )
    
    def flash_drive_backup(self):
        for device in ['sda1', 'sdb1', 'sdc1', 'sdd1']:
            p = subprocess.Popen(
                f'sudo mount /dev/{device} /mnt/usb1 -o noauto,users,rw,umask=0'.split(" "),
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                stdin=subprocess.PIPE,
            )
            if not 'does not exist' in p.communicate()[1].decode():
                print(device, 'passed')
                break

        subprocess.Popen(
            f'sudo cp -R backups/ /mnt/usb1/'.split(" "),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            stdin=subprocess.PIPE,
        )
        subprocess.Popen(
            f'sudo umount /mnt/usb1'.split(" "),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            stdin=subprocess.PIPE,
        )

    def login(self, email, passwd):
        self.client.switch_database("auth")
        if dict({"name": "users_table"}) in self.client.get_list_measurements():
            try:
                result = next(
                    self.client.query(
                        f"select * from users_table where email='{email}'"
                    ).get_points()
                )
                if bcrypt.checkpw(
                    passwd.encode("utf8"), result["password"].encode("utf-8")
                ):
                    token = self.user.generate_token(
                        result["role"], email, result["name"], result["dflt_pwd"]
                    )
                    result["token"] = token
                    self.user.update(result)
                    return token
                else:
                    return None
            except Exception as error:
                print(error)
                return False
        else:
            return False

    def check_token(self, token):
        self.client.switch_database("auth")
        if dict({"name": "users_table"}) in self.client.get_list_measurements():
            try:
                result = next(
                    self.client.query(
                        f"select * from users_table where token='{token}'"
                    ).get_points()
                )
                if result:
                    return True
                return False
            except Exception as error:
                print(error)
                return False
        else:
            return False

    def new_user(self, id, name, email, password, role, token, dflt_pwd):
        self.client.switch_database("auth")
        json_payload = []
        data = {
            "measurement": "users_table",
            "tags": {"id": id, "email": email},
            "time": datetime.now(),
            "fields": {
                "role": role,
                "name": name,
                "password": password,
                "token": token,
                "dflt_pwd": dflt_pwd,
            },
        }
        json_payload.append(data)
        self.client.write_points(json_payload)

    def remove_user(self, id):
        self.client.switch_database("auth")
        return self.client.query(f"delete from users_table where id='{id}'")

    def get_user(self, email):
        self.client.switch_database("auth")
        if dict({"name": "users_table"}) in self.client.get_list_measurements():
            try:
                return next(
                    self.client.query(
                        f'select "name","email","id","dflt_pwd","role" from users_table where email=\'{email}\''
                    ).get_points()
                )
            except Exception as error:
                print(error)
                return None
        else:
            return None

    def get_all_users(self):
        self.client.switch_database("auth")
        users = []
        try:
            if dict({"name": "users_table"}) in self.client.get_list_measurements():
                for i in self.client.query(
                    'select "name","email","id","role" from users_table'
                ).get_points():
                    users.append(i)
            return users
        except Exception as error:
            print(error)
            return []

    def update(self, user):
        try:
            self.client.switch_database("auth")
            json_payload = []
            data = {
                "measurement": "users_table",
                "tags": {"id": user["id"], "email": user["email"]},
                "time": user["time"],
                "fields": {"role": user["role"], "name": user["name"]},
            }
            if "token" in user.keys():
                data["fields"]["token"] = user["token"]
            if "password" in user.keys():
                data["fields"]["password"] = user["password"]
                data["fields"]["dflt_pwd"] = False
            if "dflt_pwd" in user.keys():
                data["fields"]["dflt_pwd"] = user["dflt_pwd"]

            json_payload.append(data)
            self.client.write_points(json_payload)
            return True
        except Exception as error:
            print(error)
            return False

    def get_silo(self, silo):
        try:
            self.client.switch_database(silo)
            if dict({"name": "general"}) in self.client.get_list_measurements():
                result = next(
                    self.client.query(
                        "select * from general order by time desc limit 1"
                    ).get_points()
                )
                result["silo"] = silo
            return result
        except Exception as error:
            print(error)
            return []

    def get_silo_configs(self):
        try:
            self.client.switch_database("configs")
            result = []
            for config in self.client.query("select * from silos").get_points():
                del config["time"]
                result.append(config)
            return sorted(result, key=lambda k: k["silo"])
        except Exception as error:
            print(error)
            return []

    def get_silos_localization(self):
        try:
            self.client.switch_database("configs")
            local = next(self.client.query("select * from localization").get_points())
            return local['localization']
        except Exception as error:
            print(error)
            return ''
    
    def post_silos_localization(self, local):
        try:
            self.client.switch_database("configs")
            json_payload = []
            data = {
                "measurement": "localization",
                "tags": {"value": 1},
                "time": 1632265794713453000,
                "fields": {"localization": local},
            }
            json_payload.append(data)
            self.client.write_points(json_payload)
            return True
        except Exception as error:
            print(error)
            return False

    def set_silo_config(self, silo, freq, local):
        try:
            self.client.create_database(silo)
            self.client.switch_database("configs")
            json_payload = []
            data = {
                "measurement": "silos",
                "tags": {"silo": silo},
                "time": 1632265794713453000,
                "fields": {"freq": freq, "local": local},
            }
            json_payload.append(data)
            self.client.write_points(json_payload)
            return True
        except Exception as error:
            print(error)
            return False

    def delete_silo_config(self, silo):
        try:
            self.client.switch_database("configs")
            self.client.query(f"delete from silos where silo='{silo}'")
            self.client.query(f'drop database "{silo}"')
            return True
        except Exception as error:
            print(error)
            return False

    def get_sensor_data(self, silo, gte, lte):
        result = {}
        self.client.switch_database(silo)
        try:
            for i in self.client.get_list_measurements():
                if "pendulo" in i["name"]:
                    resp = []
                    # vinte_tres_h = timedelta(seconds=86399)
                    gteTimestamp = (
                        int((datetime.fromtimestamp(int(gte) / 1000)).timestamp())
                        * 1000000000
                    )
                    lteTimestamp = (
                        int(
                            (
                                datetime.fromtimestamp(int(lte) / 1000)
                            ).timestamp()
                        )
                        * 1000000000
                    )
                    query = f"select * from {i['name']} where time >= {gteTimestamp} and time <= {lteTimestamp}"
                    print("query: ", query)
                    for j in self.client.query(query).get_points():
                        resp.append(j)
                    result[i["name"]] = resp
            return result
        except Exception as error:
            print(error)
            return []

    def get_general_data(self, silo, gte=None, lte=None, last_data=False):
        self.client.switch_database(silo)
        if last_data:
            try:
                resp = []
                query = f"select * from general order by time desc limit 1"
                print("query: ", query)
                resp.append(next(self.client.query(query).get_points()))
                return resp
            except Exception as error:
                print(error)
                return []
        try:
            resp = []
            # vinte_tres_h = timedelta(seconds=86399)
            gteTimestamp = (
                int((datetime.fromtimestamp(int(gte) / 1000)).timestamp()) * 1000000000
            )
            lteTimestamp = (
                int(
                    (datetime.fromtimestamp(int(lte) / 1000)).timestamp()
                )
                * 1000000000
            )
            query = f"select * from general where time >= {gteTimestamp} and time <= {lteTimestamp}"
            print("query: ", query)
            for j in self.client.query(query).get_points():
                resp.append(j)
            return resp
        except Exception as error:
            print(error)
            return []


class Silo:
    def get_silo(silo):
        session = Influx()
        if silo := session.get_silo(silo):
            return silo
        else:
            return []

    def get_all_configs():
        session = Influx()
        if configs := session.get_silo_configs():
            return configs
        else:
            return []
    
    def get_localization():
        session = Influx()
        if local := session.get_silos_localization():
            return local
        else:
            return ''
    
    def post_localization(local):
        session = Influx()
        return session.post_silos_localization(local)

    def set_new_config(silo, freq, local):
        session = Influx()
        return session.set_silo_config(silo, freq, local)

    def delete(silo):
        session = Influx()
        return session.delete_silo_config(silo)

    def get_sensor_data(silo, gte, lte):
        session = Influx()
        if data := session.get_sensor_data(silo, gte, lte):
            return data
        else:
            return []

    def get_general_data(silo, gte, lte):
        session = Influx()
        if data := session.get_general_data(silo, gte, lte):
            return data
        else:
            return []


class SiloConnection:
    # Defines
    PINO_CONFIG_MODE_RASP = 4  # pino no qual altera o modo de comunicacao do modulo de radio. Configuracao ou operacao.
    PORTA_SERIAL_RASP = "/dev/serial0"  # porta serial utilizada
    BAUDRATE_RASP = 9600  # baudrate de comunicacao
    RADIO_MODE = 1  # raspberry no modo de configuracao do radio
    OPERACAO_MODE = 0  # raspberry no modo de operacao

    ser = serial.Serial(
        PORTA_SERIAL_RASP,
        BAUDRATE_RASP,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=1,
    )

    def __init__(self):
        self.config_rasp()

    # Declara pino para selecionar configuracao do radio ou operacao. 0 = Operacao / 1 = Config Radio
    def config_rasp(self):
        try:
            GPIO.setmode(GPIO.BCM)
            GPIO.setwarnings(False)
            GPIO.setup(self.PINO_CONFIG_MODE_RASP, GPIO.OUT)
        except (Exception) as error:
            print("Error on config_rasp: ", error)


    # Configura o modo de operacao do modulo (Configuracao ou operacao)
    def set_mode_rasp(self, select_mode):
        try:
            GPIO.output(self.PINO_CONFIG_MODE_RASP, select_mode)
        except (Exception) as error:
            print("Error on set_mode_rasp: ", error)


    # Envia dados ao modulo
    def send_data_silo(self, data):
        try:
            self.ser.write(data)
        except (Exception) as error:
            print("Error on send_data_silo: ", error)
    
    # Le os dados enviados pelo silo
    def read_data_silo(self):
        try:
            buffer = []
            buffer_check_crc = []
            byte = []
            crc = []

            # Separa o tamanho do pacote recebido
            for _ in range(4):
                if aux := self.ser.read(1):
                    byte.append(aux)
                else:
                    byte.append(self.ser.read(1))
            #print('byte: ', byte)

            size_packet = byte[2] + byte[3]
            size_packet = int.from_bytes(size_packet, "big")

            #Atribui nas primeiras posicoes do buffer_check_crc o tamanho, para efetuar posteriormente o calculo do crc
            buffer_check_crc.append(int.from_bytes(byte[2], "big"))
            buffer_check_crc.append(int.from_bytes(byte[3], "big"))
            #print('buffer_check_crc_len', buffer_check_crc)

            no_data = 0
            for _ in range(size_packet):
                data = self.ser.read(1)
                if not data:
                    no_data += 1
                    if no_data > 10:
                        print('data: ', data)
                        return 'empty'
                buffer += bytes(data)
            #print('buffer', buffer)
            
            buffer_check_crc += buffer
            crc.append(buffer_check_crc[-2])
            crc.append(buffer_check_crc[-1])
            buffer_check_crc.pop(-2)
            buffer_check_crc.pop(-1)
            #print('buffer_check_crc_final', buffer_check_crc)
            #print('crc_recebido', crc)
            #print('result_CRC', result)

            if self.crc_16_check(buffer_check_crc, crc[0], crc[1]):
                print('CRC OK')
                buffer.append(size_packet)
                return buffer
            else:
                return 'crc_fail'

        except (Exception) as error:
            print("Error on read_data_silo: ", error)    

    # Transforma o valor recebido em decimal
    def converte_int_to_dec(self, valor):
        return valor / 10


    # Adquire numero de bytes de sensores habilitados / desabilitados
    def get_bytes_flag_sensores(self, total_sensores):
        num_bytes = math.ceil((total_sensores / 8))
        return num_bytes


    # Trata dados do protocolo do cliente e armazena em um json
    def trata_dados_kairos(self, data):

        data_general = []
        flags_sensores = []
        #################################################################
        # Adquire quantidade total de sensores instalados no pendulo
        total_sensores = (
            (data[16] * data[17])
            + (data[18] * data[19])
            + (data[20] * data[21])
            + (data[22] * data[23])
            + (data[24] * data[25])
        )

        # Adquire num de bytes da flag de habilitacao dos sensores
        num_bytes_flag_sensores = self.get_bytes_flag_sensores(total_sensores)

        # Adquire o ultimo termo da lista para calcular flags dos sensores
        size_packet = data.pop()

        # Adquire o index de inicio das flags dos sensores
        crc = 2
        index_flag_sens = size_packet - crc - num_bytes_flag_sensores

        for index in range(num_bytes_flag_sensores):
            flags_sensores.append(data[index_flag_sens + index])
        #################################################################
        # Dados Gerais
        # Temperatura Ambiente
        data_general.append(data[0] + self.converte_int_to_dec(data[1]))

        # Umidade
        data_general.append(data[2])

        # Maior Temperatura Medida
        data_general.append(data[3] + self.converte_int_to_dec(data[4]))

        # Valor Medio Umidade Interna
        data_general.append(data[5])

        # Tensao da Bateria - Conversao	0xdf=223. 223 * 0,0615 = 13,7V
        tensao_bateria = data[6] * 0.0615
        data_general.append(round(tensao_bateria, 1))

        # Temperatura da Parada do Motor
        # Umidade maxima permitida
        # Valor alarme de sobre temperatura
        # Produto Armazenado
        # Modo de operacao
        for index in range(5):
            data_general.append(data[7 + index])

        # Potencia aplicada ao motor. Conversao 0xFA = 250.    255 --> 100 250 --> 98%
        data_general.append(((data[12] * 100) / 255))

        # Flags de operacao
        # 0x08 = 0000 1000 => Umidade ambiente alta

        # Bit 7 = Falha de TX-RX
        # Bit 6 = Ambiente Quente
        # Bit 5 = Falha de Estacao Metereologica
        # Bit 4 = Umidade Ambiente Alta
        # Bit 3 = Chovendo
        # Bit 2 = Temperatura OK
        # Bit 1 = Horario de Ponta
        # Bit 0 = Bateria Anormal

        flag_operacao = "{0:08b}".format(data[13])
        flag_operacao = list(flag_operacao)

        # Converte string para bool
        for index in range(8):
            if flag_operacao[index] == "0":
                flag_operacao[index] = False
            else:
                flag_operacao[index] = True

        for index in range(8):
            data_general.append(flag_operacao[7 - index])

        # Flags de status
        # 0x06 = 0000 0110 => Fluxo de ar e sensor de chuva ativo

        # Bit 6 = Sinaliza motor ligado
        # Bit 5 = Tipo de controle (1 Fluxo de Ar / 0 Produto)
        # Bit 4 = Sinaliza sensor de chuva ativo(1) / inativo (0)
        # Bit 3 = Alarme de sobretemperatura dos graos (1) ou (0)
        # Bit 2 = Sinaliza falha dos sensores (1) ou (0)
        # Bit 1 = Relogio reajustado
        # Bit 0 = Retorno da energia

        flag_sts = "{0:07b}".format(data[14])
        flag_sts = list(flag_sts)

        # Converte string para bool
        for index in range(7):
            if flag_sts[index] == "0":
                flag_sts[index] = False
            else:
                flag_sts[index] = True

        for index in range(7):
            data_general.append(flag_sts[6 - index])
            
        #################################################################
        # Quantidade de pendulos no silo
        data_general.append(data[15])

        habilitacao_sensores = []
        for index in range(num_bytes_flag_sensores):
            valor = "{0:08b}".format(flags_sensores[index])
            habilitacao_sensores.append(valor)

        estado_sensores = []
        sensores_habilitados = []
        for index in range(num_bytes_flag_sensores):
            estado_sensores.append(list(habilitacao_sensores[index]))

        count_list = 0
        count_i = 0
        for index in range(total_sensores):
            sensores_habilitados.append(estado_sensores[count_list][count_i])
            count_i += 1
            if count_i == 8:
                count_i = 0
                count_list += 1
        # print(sensores_habilitados)

        # Converte string para bool
        for index in range(total_sensores):
            if sensores_habilitados[index] == "0":
                sensores_habilitados[index] = False
            else:
                sensores_habilitados[index] = True
        # print()
        # print(sensores_habilitados)

        count_sensores_habilitados = 0
        count = 0

        # Modelo de pendulo A
        if data[16] > 0:  # Verifica se ha pendulos do modelo A
            i = 1
            for index in range(data[16] * 16):
                if data[17] == 16:  # 16 sensores
                    data_general.append(
                        data[31 + count] + self.converte_int_to_dec(data[32 + count])
                    )
                    data_general.append(sensores_habilitados[count_sensores_habilitados])
                    count_sensores_habilitados += 1
                    count += 2
                else:
                    if i <= data[17]:
                        data_general.append(
                            data[31 + count] + self.converte_int_to_dec(data[32 + count])
                        )
                        data_general.append(
                            sensores_habilitados[count_sensores_habilitados]
                        )
                        count_sensores_habilitados += 1
                        count += 2
                        i += 1
                    else:
                        data_general.append(0)
                        data_general.append("ND")
                        i += 1
                        if i == 17:
                            i = 1

        # Modelo de pendulo B
        if data[18] > 0:  # Verifica se ha pendulos do modelo B
            i = 1
            for index in range(data[18] * 16):
                if data[19] == 16:  # 16 sensores
                    data_general.append(
                        data[31 + count] + self.converte_int_to_dec(data[32 + count])
                    )
                    data_general.append(sensores_habilitados[count_sensores_habilitados])
                    count_sensores_habilitados += 1
                    count += 2
                else:
                    if i <= data[19]:
                        data_general.append(
                            data[31 + count] + self.converte_int_to_dec(data[32 + count])
                        )
                        data_general.append(
                            sensores_habilitados[count_sensores_habilitados]
                        )
                        count_sensores_habilitados += 1
                        count += 2
                        i += 1
                    else:
                        data_general.append(0)
                        data_general.append("ND")
                        i += 1
                        if i == 17:
                            i = 1

        # Modelo de pendulo C
        if data[20] > 0:  # Verifica se ha pendulos do modelo C
            i = 1
            for index in range(data[20] * 16):
                if data[21] == 16:  # 16 sensores
                    data_general.append(
                        data[31 + count] + self.converte_int_to_dec(data[32 + count])
                    )
                    data_general.append(sensores_habilitados[count_sensores_habilitados])
                    count_sensores_habilitados += 1
                    count += 2
                else:
                    if i <= data[21]:
                        data_general.append(
                            data[31 + count] + self.converte_int_to_dec(data[32 + count])
                        )
                        data_general.append(
                            sensores_habilitados[count_sensores_habilitados]
                        )
                        count_sensores_habilitados += 1
                        count += 2
                        i += 1
                    else:
                        data_general.append(0)
                        data_general.append("ND")
                        i += 1
                        if i == 17:
                            i = 1

        # Modelo de pendulo D
        if data[22] > 0:  # Verifica se ha pendulos do modelo D
            i = 1
            for index in range(data[22] * 16):
                if data[23] == 16:  # 16 sensores
                    data_general.append(
                        data[31 + count] + self.converte_int_to_dec(data[32 + count])
                    )
                    data_general.append(sensores_habilitados[count_sensores_habilitados])
                    count_sensores_habilitados += 1
                    count += 2
                else:
                    if i <= data[23]:
                        data_general.append(
                            data[31 + count] + self.converte_int_to_dec(data[32 + count])
                        )
                        data_general.append(
                            sensores_habilitados[count_sensores_habilitados]
                        )
                        count_sensores_habilitados += 1
                        count += 2
                        i += 1
                    else:
                        data_general.append(0)
                        data_general.append("ND")
                        i += 1
                        if i == 17:
                            i = 1

        # Modelo de pendulo E
        if data[24] > 0:  # Verifica se ha pendulos do modelo E
            i = 1
            for index in range(data[24] * 16):
                if data[25] == 16:  # 16 sensores
                    data_general.append(
                        data[31 + count] + self.converte_int_to_dec(data[32 + count])
                    )
                    data_general.append(sensores_habilitados[count_sensores_habilitados])
                    count_sensores_habilitados += 1
                    count += 2
                else:
                    if i <= data[25]:
                        data_general.append(
                            data[31 + count] + self.converte_int_to_dec(data[32 + count])
                        )
                        data_general.append(
                            sensores_habilitados[count_sensores_habilitados]
                        )
                        count_sensores_habilitados += 1
                        count += 2
                        i += 1
                    else:
                        data_general.append(0)
                        data_general.append("ND")
                        i += 1
                        if i == 17:
                            i = 1
        #######################################################################
        # Sensores Habilitados / Desabilitados
        # print("Flag Sensores")
        # print(flags_sensores)
        # print()
        #######################################################################
        # Timestamp
        # Dia
        data_general.append(data[26])

        # Mes
        data_general.append(data[27])

        # Ano
        data_general.append(data[28])

        # Hora
        data_general.append(data[29])

        # Minuto
        data_general.append(data[30])
        #######################################################################
        return data_general

    # Coleta e trata os dados recebidos do silo. Apos realizar o tratamento, envia os dados ao banco
    def coletar_data(self, freq):
        print('freq: ', freq)
        # Configuracao do modulo de radio para comunicacao. Default eh 0x28. Apos alterar configuracao, aguardar 10ms.
        altera_freq_comunicacao_silo = [
            0xC2,
            0x05,
            0x01,
            int(freq),
        ]
        # Comando para solicitar os dados do silo
        solicita_data_silo = [0xb5, 0xc3]

        # Altera a frequencia de comunicacao
        print('set_mode_rasp')
        self.set_mode_rasp(self.RADIO_MODE)
        time.sleep(0.1)
        print('send_data_silo')
        print('list: ', altera_freq_comunicacao_silo)
        self.send_data_silo(altera_freq_comunicacao_silo)
        print('set_mode_rasp')
        self.set_mode_rasp(self.OPERACAO_MODE)  # Muda para modo de operacao
        time.sleep(0.1)

        self.ser.flushInput()
        self.ser.flushOutput()
        time.sleep(0.1)

        # Solicita dados do silo
        print('send_data_silo')
        self.send_data_silo(solicita_data_silo)
        #time.sleep(0.5)  # tempo de resposta do silo
        time.sleep(1)

        # Le dados do silo
        print('read_data_silo')
        crc_errors = 0                      
        for _ in range(3):
            data_read = self.read_data_silo()
            if isinstance(data_read, str):
                if data_read == 'crc_fail':
                    crc_errors += 1
                    if crc_errors > 2:
                        raise Exception('CRC Failure')
                if data_read == 'empty':
                    raise Exception('Empty Data')
            else:
                break
        
        #else:
            #raise Exception('data menor do que deveria')
            #print('>>>>>>>>>>>>>>>>>>>', data_read)
        # print('>>>>>>>>>>>>>>>>>>>', data_read)
        # print()
        print('trata_dados_kairos')
        json_payload = self.trata_dados_kairos(data_read)
        return json_payload

    #Calcula e verifica o CRC do buffer recebido
    def crc_16_check(self, buffer_recebido, crc_msb, crc_lsb):
        try:
            #Armazena o crc recebido no pacote
            size = len(buffer_recebido)

            #Calcula o crc do pacote recebido
            crc_check = 0xFFFF
            for i in range(size):
                crc_check ^= buffer_recebido[i]
                for j in range(8): # Faz o loop para cada bit
                    if ((crc_check & 0x0001) != 0):
                        crc_check >>= 1
                        crc_check ^= 0x8005
                    else:
                        crc_check >>= 1
            crc_msb_calc = (crc_check & 0x00FF)
            crc_lsb_calc = ((crc_check>>8) & 0x00FF)

            #Compara o crc recebido com o calculado
            if((crc_msb_calc == crc_msb) and (crc_lsb_calc == crc_lsb)):
                #print("CRC OK")
                return True
            else:
                #print("CRC FAIL")
                return False
        except (Exception) as error:
            print("Error in verify CRC-16: ", error)
            return None