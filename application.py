from handler import threading_collect, threading_backup
from config import settings
from initial_config import admin_setup
from routes import setup_routes
from aiohttp import web
from log_config import configure

configure("api-auth")
# setup app
app = web.Application()
app = setup_routes(app)

admin_setup()
threading_collect()
threading_backup()

# descomentar para roda em ambiente de dev
if __name__ == "__main__":
    web.run_app(app, host=settings.API_HOST, port=settings.API_PORT)
