from dynaconf import Dynaconf
import subprocess

settings = Dynaconf(
    envvar_prefix="DYNACONF",
    settings_files=["./settings.toml", "./settings/.secrets.toml"],
)

# `envvar_prefix` = export envvars with `export DYNACONF_FOO=bar`.
# `settings_files` = Load this files in the order.
