FROM python:buster AS base
WORKDIR /opt/app
COPY requirements.txt .
RUN pip install -r requirements.txt

FROM base AS staging
COPY controllers ./controllers
COPY models ./models
COPY handler ./handler
# COPY settings ./settings
COPY __main__.py config.py initial_config.py log_config.py routes.py settings.toml ./
EXPOSE 9000
ENTRYPOINT [ "python","__main__.py" ]
