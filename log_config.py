from datetime import datetime
from logging.handlers import TimedRotatingFileHandler
from os import mkdir, path
from time import gmtime
import logging


def configure(filename: str = "logs"):
    _create_log_folder()

    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    date_format = "%d-%m-%Y"
    log_dir = "log/{}.log".format(filename)

    console_handler = logging.StreamHandler()
    # file_handler = TimedRotatingFileHandler(
    #     log_dir,
    #     when="midnight",
    #     interval=1,
    #     utc=True,
    #     backupCount=9,
    # )

    formatter = logging.Formatter(
        "%(asctime)s - %(levelname)s - %(message)s", date_format + " %H:%M:%S"
    )
    formatter.converter = gmtime

    console_handler.setFormatter(formatter)
    # file_handler.setFormatter(formatter)

    logger.handlers = []

    logger.addHandler(console_handler)
    # logger.addHandler(file_handler)


def _create_log_folder():
    folder_name = "log"

    if not path.exists(folder_name):
        mkdir(folder_name)
